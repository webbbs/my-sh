sshlogin() {
    if [ -z "$1" ]; then
        echo "Usage: $0 user@host"
        return 1
    fi

    ssh "$1" 'mkdir -p ~/.ssh'
    cat ~/.ssh/id_rsa.pub | ssh $1 'cat >> ~/.ssh/authorized_keys'
    ssh $1 chmod 700 .ssh
    ssh $1 chmod 600 .ssh/authorized_keys
}

pon() {
    p="http://127.0.0.1:1086"
    export {http,https}_proxy="$p"
    export {HTTP,HTTPS}_PROXY="$p"
    export no_proxy="127.0.0.1,.byted.org,.bytedance.net"
    
    pshow
}

poff() {
    unset http_proxy;
    unset https_proxy;
    unset HTTP_PROXY;
    unset HTTPS_PROXY;
    unset no_proxy;
    pshow
}

pshow() {
    echo "export http_proxy=\"$http_proxy\""
    echo "export https_proxy=\"$https_proxy\""
    echo "export HTTP_PROXY=\"$HTTP_PROXY\""
    echo "export HTTPS_PROXY=\"$HTTPS_PROXY\""
    echo "export no_proxy=\"$no_proxy\""
}

function fh() {
    if [[ -z $1 ]]; then
        echo 'input fh'
        return 1
    fi
    http "https://fh4g.com/api/search?query=$1&page=0"
}
 
function md2html() {
    pandoc -f markdown -t html5  $1 | pbcopy
    echo 'content copied to clipboard'
}


# 自定义
function floo()
{
    # export DOCKER_HOST=tcp://172.16.21.180:2376
    echo "find docker containers:"
    docker ps | grep "$1"
    linenumber=`docker ps | grep "$1"  | wc -l | tr -d ' '`
    if [ "$linenumber" -eq "1" ]; then
        docker exec -it `docker ps | grep "$1" | awk '{print $1}'` /bin/bash
    fi
    # unset DOCKER_HOST
}

