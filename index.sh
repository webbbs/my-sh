test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

fpath=(~/.zsh/completion $fpath)
autoload -Uz compinit && compinit -i

# autojump
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

# bash completion
autoload -U +X bashcompinit && bashcompinit
source /usr/local/etc/bash_completion.d/*
source /usr/local/etc/profile.d/z.sh
complete -o nospace -C /usr/local/bin/vault vault

# shell
alias ll='ls -lh'
alias la='ls -lah'
alias py_http='python -m SimpleHTTPServer'
alias zshrc='vim ~/.my-sh/index.sh'

alias http='http --verify=no'
alias diff='icdiff'
alias myip='http ifconfig.co'

alias ssh='ssh -A'
alias vi='vim'
# fix 中文乱码
alias tree='tree -N'

alias getdns='networksetup -getdnsservers WI-FI'
alias setdns='networksetup -setdnsservers WI-FI 172.16.21.20 223.5.5.5 && networksetup -getdnsservers WI-FI'
alias apb='ansible-playbook -v'
alias show_listen='lsof -nP -i TCP  |grep -i listen'

alias devbox='ssh devbox'
alias nas='ssh nas' 
alias sshrouter="ssh 18600110665@192.168.1.1"

# vagrant
alias vgss='vagrant snapshot'
alias vmcentos='cd ~/projects/vms/centos && vagrant up && vagrant ssh'
alias vmubuntu='cd ~/projects/vms/ubuntu && vagrant up && vagrant ssh'

# mvn
alias mvnpkg='mvn clean package -Dmaven.test.skip=true'
alias mvndpy='mvn clean deploy -Dmaven.test.skip=true'


# k8s
alias 'pod'='kubectl -n test get po'
alias 'logs'='kubectl -n test logs -f'
alias 'kexec'='kubectl -n test exec -it'


source ~/.my-sh/funcs.sh


export LC_ALL=en_US.UTF-8  
export LANG=en_US.UTF-8

# mvn
export M2_HOME=/usr/local/opt/maven/libexec
# export NEXUS_USERNAME='admin'
# export NEXUS_PASSWORD='9e79HRDyDAadYg3G'

# btrace
export BTRACE_HOME=/usr/local/opt/btrace
export PATH=$BTRACE_HOME/bin:$PATH

export HOMEBREW_GITHUB_API_TOKEN=7a9f7f435010939da72b357860c4d07eb0a55f96
export HOMEBREW_NO_AUTO_UPDATE=1

export CLOUDCONVERT_API_KEY="BcpuLMm2YLZZWJwocr_pypyMzLT4kJB-GYZheop66YW5UJYN0dv8gz7BCjRurV11X2agkk0znX8v3eHFR0zrvQ"
export VULTR_API_KEY='QRUK3ESOSVQHBVDPLEURDWAFQH6KLGOE2YIA'

# android
export ANDROID_HOME=/Users/$USER/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

# gnu utils
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
export MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
export PATH="/usr/local/opt/binutils/bin:$PATH"
export PATH="$GOPATH/bin:/usr/local/sbin:/usr/local/bin:$PATH"
export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"
export PATH="/usr/local/opt/redis@4.0/bin:$PATH"
export PATH="/usr/local/opt/ruby/bin:$PATH"

export SSLKEYLOGFILE=~/.tls/sslkeylog.log

export PYTHONWARNINGS="ignore:DEPRECATION"

